from django.shortcuts import redirect, render
from receipts.forms import AccountForm, ExpenseCategoryForm, ReceiptForm

from receipts.models import Account, ExpenseCategory, Receipt

from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def receipt_list(request):
    receipt = Receipt.objects.all().filter(purchaser=request.user)
    context = {
        "receipt_list": receipt,
    }

    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

        # Makes sure each uniqie user can only see the category and account they made
        form.fields["category"].queryset = ExpenseCategory.objects.filter(
            owner=request.user
        )
        form.fields["account"].queryset = Account.objects.filter(
            owner=request.user
        )

    context = {"form": form}

    return render(request, "receipts/create.html", context)


@login_required
def expense_category_list(request):
    expense_category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "expense_category": expense_category,
    }

    return render(request, "receipts/expense_category.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}

    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expense_category = form.save(False)
            expense_category.owner = request.user
            expense_category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {"form": form}

    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}

    return render(request, "receipts/create_account.html", context)
